package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onClickCalculate(View view) {
        EditText first = findViewById(R.id.first);
        EditText second = findViewById(R.id.second);

        try {
            String temp = first.getText().toString();
            Integer fstNumber = Integer.parseInt(temp);
            temp = second.getText().toString();
            Integer sndNumber = Integer.parseInt(temp);

            Integer result = fstNumber + sndNumber;

            Intent intent = new Intent(this, SecondActivity.class);
            intent.putExtra("First", fstNumber);
            intent.putExtra("Second", sndNumber);
            intent.putExtra("Result", result);
            startActivity(intent);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "ВВЕДЕНЫ НЕВЕРНЫЕ ЗНАЧЕНИЯ", Toast.LENGTH_LONG).show();
        }
    }
}