package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView textView = findViewById(R.id.expression);

        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            Integer fstNumber = arguments.getInt("First");
            Integer sndNumber = arguments.getInt("Second");
            Integer result = arguments.getInt("Result");

            if (sndNumber >= 0)  {
                textView.setText(fstNumber + " + " + sndNumber + " = " + result);
            }
            else {
                textView.setText(fstNumber + " + " + sndNumber + ") = " + result);
            }
        }
    }
}